#include "stdio.h"
#include "stdlib.h"
#include <algorithm>
#include "Array2D.cpp"

using namespace std;//for min and max functions



//BEGIN BST -----------------------------------------------------------------

class Node {
	//left/right/top/bottomCoord are bounding box of a partition of space, roomLeft/right/top/bottom are bounding box of the room in that partition
    int leftCoord, rightCoord, topCoord, bottomCoord, roomLeft, roomRight, roomTop, roomBottom;
    Node* left;
    Node* right;
    Node* parent;
public:
    Node() { leftCoord=-1; rightCoord=-1; topCoord=-1; bottomCoord=-1; roomLeft=-1; roomRight=-1; roomTop=-1; roomBottom=-1; left=NULL; right=NULL; parent = NULL;};
    void setLeftCoord(int left) { leftCoord = left; };
	void setRightCoord(int right) { rightCoord = right; };
	void setTopCoord(int top) { topCoord = top; };
	void setBottomCoord(int bottom) { bottomCoord = bottom; };
	void setRoomLeft(int theLeft) {roomLeft = theLeft;};
	void setRoomRight(int theRight) {roomRight = theRight;};
	void setRoomTop(int theTop) {roomTop = theTop;};
	void setRoomBottom(int theBottom) {roomBottom = theBottom;};
    void setLeft(Node* aLeft) { left = aLeft; };
    void setRight(Node* aRight) { right = aRight; };
    void setParent(Node* aParent) { parent = aParent; };
    int LeftCoord() { return leftCoord; };
	int RightCoord() { return rightCoord; };
	int TopCoord() { return topCoord; };
	int BottomCoord() { return bottomCoord; };
	int RoomLeft() {return roomLeft;};
	int RoomRight() {return roomRight;};
	int RoomTop() {return roomTop;};
	int RoomBottom() {return roomBottom;};
    Node* Left() { return left; };
    Node* Right() { return right; };
    Node* Parent() { return parent; };
};
/*
// Binary Tree class
class Tree {
    Node* root;
public:
    Tree();
    ~Tree();
    Node* Root() { return root; };
    
   
private:
    
    void freeNode(Node* leaf)
	{
		if ( leaf != NULL ) 
		{
			freeNode(leaf->Left());
			freeNode(leaf->Right());
			delete leaf;
		}
	}
};

// Constructor
Tree::Tree() {
    root = NULL;
}

// Destructor
Tree::~Tree() {
    freeNode(root);
}

*/
//END BT ------------------------------------------------------------



static class Level
{
public:
	//function declarations
	//static Node* bstGenerate(Array2D<char> &level, int top, int bottom, int left, int right, Node &root);
	//static void makePaths(Array2D<char> &level, Node &root);
	//static int countLeaves(Node &root);

	//recursively generates a dungeon layout using space partitioning, given the character array 'level' and the top left and bottom right corner indices, and the parent node
	static Node* bstGenerate(Array2D<char> &level, int top, int bottom, int left, int right, Node &root)
	{
		int minRoomSize = 5; //smallest allowed dimensions of a room
		Node* curr = new Node();
	
		Node* leftChild;
		Node* rightChild;
		int width = right-left+1;
		int height = bottom-top+1;
		//if area isn't too small
		if(width>2.2*(minRoomSize+2) && height>2.2*(minRoomSize+2))
		{
			//chose a split direction
			int splitDir = rand() % 2;
			int splitPos;
		
			if(splitDir) //vertical split
			{
				//pick a split point, in the middle 10 percent
			
				splitPos = (rand() % (width / 10 + 1)) + (int)(width * 0.45);
				//call on each new section
				leftChild = bstGenerate(level, top, bottom, left, left+splitPos, *curr); //left section
				rightChild = bstGenerate(level, top, bottom, left+splitPos+1, right, *curr); //right section
			}
			else //horizontal split
			{
				//pick a split point, in the middle 10 percent
				splitPos = (rand() % (height / 10 +1)) + (int)(height * 0.45);
				//call on each new section
				leftChild = bstGenerate(level, top, top+splitPos, left, right, *curr); //top section
				rightChild = bstGenerate(level, top+splitPos+1, bottom, left, right, *curr); //bottom section
			}
			(*curr).setLeft(leftChild);
			(*curr).setRight(rightChild);
			//for non-leaf nodes, roomLeft/Right/Top/Bottom hold the bounding box of all the rooms within their leaves
			(*curr).setRoomLeft(min((*leftChild).RoomLeft(),(*rightChild).RoomLeft()));
			(*curr).setRoomRight(max((*leftChild).RoomRight(),(*rightChild).RoomRight()));
			(*curr).setRoomTop(min((*leftChild).RoomTop(),(*rightChild).RoomTop()));
			(*curr).setRoomBottom(max((*leftChild).RoomBottom(),(*rightChild).RoomBottom()));
		
		}
		else //region is too small to be further subdivided
		{
			int roomWidth, roomHeight, roomTop, roomLeft;
			//generate room dimensions that fit into region
		
				roomWidth = (rand() % (width - minRoomSize - 1))+ minRoomSize;
		
				roomHeight = (rand() % (height - minRoomSize - 1))+ minRoomSize;
			//generate random room position (top left corner) that would allow room to fit given its size
			roomLeft = (rand() % (width - roomWidth - 1)) + left + 1;
			roomTop = (rand() % (height - roomHeight - 1)) + top + 1;
			for(int i = left; i <= right; i++)
			{
				for(int j = top; j <= bottom; j++)
				{
				
					if(i>=roomLeft && i<roomLeft+roomWidth && j>=roomTop && j<roomTop+roomHeight)
					{

						level(i,j) = ' ';
					}
					else
						level(i,j) = 'X';
				}
			}
	
			(*curr).setRoomLeft(roomLeft);
			(*curr).setRoomRight(roomLeft+roomWidth-1);
			(*curr).setRoomTop(roomTop);
			(*curr).setRoomBottom(roomTop+roomHeight-1);
			/*start printmap 	
			for(int i = 0; i<gridHieght; i++)
			{
				for(int j=0; j<gridWidth; j++)
				{
					printf("%c", level(i, j));
				}
				printf("\n");
			}
			printf("****************************************************************\n");
			end printmap*/
		}

		(*curr).setLeftCoord(left);
		(*curr).setRightCoord(right);
		(*curr).setTopCoord(top);
		(*curr).setBottomCoord(bottom);
	
		(*curr).setParent(&root);
		return curr;
		//end
	}

	//creates pathways connecting rooms that are siblings in a BT
	static void makePaths(Array2D<char> &level, Node &root)
	{

		//if there are more subdivisions in this tree
		if(countLeaves(*(root.Left())) + countLeaves(*(root.Right())) > 2)
		{
			//recur in largest subtree first
			if(countLeaves(*(root.Left())) > countLeaves(*(root.Right())))
			{
				makePaths(level, *(root.Left()));
				makePaths(level, *(root.Right()));
			}
			else
			{
				makePaths(level, *(root.Right()));
				makePaths(level, *(root.Left()));	
			}
		
		}
	
	
		int startX, startY; 
		int leftCenterX, leftCenterY, rightCenterX, rightCenterY;
	
		//centers of the rooms to be joined
		rightCenterX = (*root.Right()).RoomLeft() + ((*root.Right()).RoomRight() - (*root.Right()).RoomLeft())/2;
		leftCenterX = (*root.Left()).RoomLeft() + ((*root.Left()).RoomRight() - (*root.Left()).RoomLeft())/2;
		rightCenterY = (*root.Right()).RoomTop() + ((*root.Right()).RoomBottom() - (*root.Right()).RoomTop())/2;
		leftCenterY = (*root.Left()).RoomTop() + ((*root.Left()).RoomBottom() - (*root.Left()).RoomTop())/2;

		//if horizontal split
		if((*root.Left()).TopCoord() != (*root.Right()).TopCoord())
		{
			bool stop = false;
			bool direction = true;
		
			startY = (*root.Left()).BottomCoord();
		
			startX = root.LeftCoord() + (root.RightCoord() - root.LeftCoord()) / 2;

			while(!stop)
			{
				if(level(startX, startY) == 'X')
				{
					level(startX, startY) = ' ';
				}
				else
				{
					stop = true;
				}

				if(direction)
				{
					startY--;
					if(startY == leftCenterY && (startX > (*root.Left()).RoomRight() || startX < (*root.Left()).RoomLeft()))
					{
						direction = false;
					}
				}
				else
				{
					if(startX > (*root.Left()).RoomRight())
					{
						startX--;
					}
					else
					{
						startX++;
					}
				}
			}
			//right node
		
			stop = false;
			direction = true;
			startY = (*root.Right()).TopCoord();
		
			startX = root.LeftCoord() + (root.RightCoord() - root.LeftCoord()) / 2;
		

			while(!stop)
			{
				if(level(startX, startY) == 'X')
				{
					level(startX, startY) = ' ';
				}
				else
				{
					stop = true;
				}

				if(direction)
				{
					startY++;
					if(startY == rightCenterY && (startX > (*root.Right()).RoomRight() || startX < (*root.Right()).RoomLeft()))
					{
						direction = false;
					}
				}
				else
				{
					if(startX > (*root.Right()).RoomRight())
					{
						startX--;
					}
					else
					{
						startX++;
					}
				}
			}
		
		}
		else //else vertical split
		{
			bool stop = false;
			bool direction = true;
			startY = root.TopCoord() + (root.BottomCoord() - root.TopCoord()) / 2;
		
			startX = (*root.Left()).RightCoord();

			while(!stop)
			{
				if(level(startX, startY) == 'X')
				{
					level(startX, startY) = ' ';
				}
				else
				{
					stop = true;
				}

				if(direction)
				{
					startX--;
					if(startX == leftCenterX && (startY > (*root.Left()).RoomBottom() || startY < (*root.Left()).RoomTop()))
					{
						direction = false;
					}
				}
				else
				{
					if(startY > (*root.Left()).RoomBottom())
					{
						startY--;
					}
					else
					{
						startY++;
					}
				}
			}
			//right node
		
			stop = false;
			direction = true;
			startY = root.TopCoord() + (root.BottomCoord() - root.TopCoord()) / 2;
			startX = (*root.Right()).LeftCoord();
		

			while(!stop)
			{
				if(level(startX, startY) == 'X')
				{
					level(startX, startY) = ' ';
				}
				else
				{
					stop = true;
				}

				if(direction)
				{
					startX++;
					if(startX == rightCenterX && (startY > (*root.Right()).RoomBottom() || startY < (*root.Right()).RoomTop()))
					{
						direction = false;
					}
				}
				else
				{
					if(startY > (*root.Right()).RoomBottom())
					{
						startY--;
					}
					else
					{
						startY++;
					}
				}
			}
		}
		
	
	}

	static int countLeaves(Node &root)
	{
		int count = 0;
		if(root.Left() != NULL && root.Right() != NULL)
		{
			count = count + countLeaves(*(root.Left())) + countLeaves(*(root.Right()));
		}
		else
			count++;
		return count;
	}
};