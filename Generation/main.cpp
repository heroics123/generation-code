#include "stdio.h"
#include "stdlib.h"
#include <time.h>
#include "Level.cpp"






//total grid dimensions
	int gridWidth = 50;
	int gridHieght = 50;
	







int main(int argc, char **argv)
{
	


	//the map representation, an abstract 2D array class
	Array2D<char> grid(gridWidth, gridHieght);
	//This seeds the generator using the system clock; each time the program is run different rooms will be generated.
	srand(time(NULL));



	//generate the dungeon rooms using space partitioning
	Node aroot;
	aroot.setLeft(Level::bstGenerate(grid, 0, grid.GetHt()-1, 0, grid.GetWd()-1, aroot));
	Level::makePaths(grid, (*aroot.Left()));
	
	//prints map representation
	for(int i = 0; i<gridHieght; i++)
	{
		for(int j=0; j<gridWidth; j++)
		{
			printf("%c", grid(j, i));
		}
		printf("\n");
	}
}


